// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  BASE_URL: 'https://api.itjewelers.com/ITJ/api/v1',
  // BASE_URL: 'http://localhost:8000/ITJ/api/v1',

  FEEDS: {
    // BASE_URL: 'http://localhost:7000/ITJ/api/v1',
    // BASE_URL: ' https://hzrdqqkj75.execute-api.us-east-1.amazonaws.com/dev/ITJ/api/v1',
      BASE_URL: 'https://pwol7othd5.execute-api.us-east-1.amazonaws.com/prod/ITJ/api/v1', // Production url
      // BASE_URL: ' https://hzrdqqkj75.execute-api.us-east-1.amazonaws.com/dev/ITJ/api/v1',  // Testing url
      // BASE_URL: 'https://d937ac1d9e78.ngrok.io/ITJ/api/v1',
    BLOG_URL: '/blogs/feed',
    // FILTER_URL: '/blogs/filter'
  },
  baseHref: '/',
  vimeo: {
     token: '04a4cc57d39928dd987ea208d88363c9', //itj
    URL: 'https://api.vimeo.com/me/videos'
  },
  cloudinary: {
    upload: {

      // **************************** For Production Only */
      cloud_name: 'itjewelers-llc', // Your cloud name 'itjimage'
      // cloud_name: 'padwalandpadwalsons',
      upload_preset: 'ITJProd-image', // Your preset
      upload_Vpreset: 'ITJProd-video', // Upload video Preset
      upload_raw: 'ITJProd-raw'
    },
    // **************************** For Production Only */
    // baseApiUrl: 'https://api.cloudinary.com/v1_1/itjewelers-llc/image/upload',
    // baseApiVidUrl: 'https://api.cloudinary.com/v1_1/itjewelers-llc/video/upload',
    // baseRawUrl: 'https://api.cloudinary.com/v1_1/itjewelers-llc/raw/upload'
    baseApiUrl: 'https://api.cloudinary.com/v1_1/padwalandpadwalsons/image/upload',
    baseApiVidUrl: 'https://api.cloudinary.com/v1_1/padwalandpadwalsons/video/upload',
    baseRawUrl: 'https://api.cloudinary.com/v1_1/padwalandpadwalsons/raw/upload'

  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
