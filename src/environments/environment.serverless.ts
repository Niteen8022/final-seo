export const environment = {
  production: true,
  BASE_URL: 'https://api1.itjewelers.com/ITJ/api/v1',
  FEEDS: {
    BASE_URL: 'https://hzrdqqkj75.execute-api.us-east-1.amazonaws.com/dev/ITJ/api/v1',
    BLOG_URL: '/blogs/feed'
  },
  baseHref: '/',
  vimeo: {
    URL: 'https://api.vimeo.com/me/videos',
    token: '04a4cc57d39928dd987ea208d88363c9',
  },
  cloudinary: {
    upload: {
      // **************************** For Production Only */
      cloud_name: 'itjewelers-llc', // Your cloud name 'itjimage'
      upload_preset: 'ITJProd-image', // Your preset
      upload_Vpreset: 'ITJProd-video', // Upload video Preset
      upload_raw: 'ITJProd-raw'
    },
    // **************************** For Production Only */
    baseApiUrl: 'https://api.cloudinary.com/v1_1/itjewelers-llc/image/upload',
    baseApiVidUrl: 'https://api.cloudinary.com/v1_1/itjewelers-llc/video/upload',
    baseRawUrl: 'https://api.cloudinary.com/v1_1/itjewelers-llc/raw/upload'

    // *************************** for TEST Server Only ***//

    // baseApiUrl: 'https://api.cloudinary.com/v1_1/padwalandpadwalsons/image/upload',
    // baseApiVidUrl : 'https://api.cloudinary.com/v1_1/padwalandpadwalsons/video/upload',
    // baseRawUrl: 'https://api.cloudinary.com/v1_1/padwalandpadwalsons/raw/upload'
  },

};
