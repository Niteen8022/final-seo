import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { environment } from '../environments/environment';
import { isPlatformBrowser, DOCUMENT } from '@angular/common';
import { Router } from '@angular/router'
import { Meta, Title } from '@angular/platform-browser';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
      title = 'marketplace';
    public ngOnInit(): void {
        this.updateTitle('Best Ecommerce SEO Company in California | ITJ Group')

        if (!isPlatformBrowser(this.platformId)) {
            const bases = this.document.getElementsByTagName('base');

            if (bases.length > 0) {
                bases[0].setAttribute('href', environment.baseHref);
            }
        }
    }


    constructor(@Inject(PLATFORM_ID) private platformId: any, @Inject(DOCUMENT) private document: any, public router: Router,
        private titleService: Title, private meta: Meta, ) {
        this.meta.updateTag({ name: 'description', content: "ITJ Group based in LA, California provides the best SEO services & web development services for Jewelers. We ensure a compelling, lucrative online presence" });
        this.meta.updateTag({ name: 'robots', content: 'index, follow' });
        this.meta.updateTag({ name: 'language', content: 'English' });
        this.meta.updateTag({ name: 'type', content: 'ITJewelers' });
        this.meta.updateTag({ name: 'revisit-after', content: '1 days' });
        this.meta.updateTag({ name: 'site_name', content: 'ITJewelers' });
        this.meta.updateTag({ name: 'keywords', content: "seo services near me, seo companies california, best ecommerce seo company, web development california, app developers in california" })
    }
    updateTitle(title: string) {
        this.titleService.setTitle(title);
    }
}
