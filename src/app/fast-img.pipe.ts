import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'fastImg' })
export class FastImgPipe implements PipeTransform {
  transform(value: string): string {
    if (value === undefined || value=== null) {
      return value;
    } else if (value.indexOf('c_scale') > 0) {
      return value.replace('upload/', 'upload/f_auto,');
    } else if (value.indexOf('/upload') > 0) {
      return value.replace('upload/', 'upload/f_auto/');
    } else {
      return value;
    }
  }
}