import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
 declarations: [
   AppComponent,
 ],
 imports:[
 CommonModule,
NgtUniversalModule,
   AppRoutingModule,
   HttpClientModule,
   BrowserModule.withServerTransition({
    appId: 'ng-universal-demystified'
})
 ],
 providers: [],
})
export class AppModule { }



