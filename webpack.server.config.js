// Work around for https://github.com/angular/angular-cli/issues/7200

const path = require('path');
const webpack = require('webpack');


module.exports = {
  mode: 'development',
  entry: {
    // This is our Express server for Dynamic universal
    server: './server.ts',
	prerender: './prerender.ts'
  },
  // externals: [nodeExternals()],
  // externals: [/node_modules/ ],
  externals: {
    
    // './dist/server/main': 'require("./dist/server/main")'
  },
  target: 'node',
  resolve: { extensions: ['.ts', '.js', '.node'] },
  optimization: {
    minimize: false
  },
  output: {
  libraryTarget: 'commonjs2',
  // externals: {
	// 	canvas: "commonjs2 canvas" // Important (2)
	// },
  // externals: [nodeExternals()],
//   externals: {
//        canvas:{}
//  },
    // Puts the output at the root of the dist folder
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
  },
  // externals: {
	// 	canvas: "commonjs2 canvas" // Important (2)
	// },
  
  module: {
    noParse: /polyfills-.*\.js/,
    rules: [
      { test: /\.ts$/, loader: 'ts-loader'  },
      {
        // Mark files inside `@angular/core` as using SystemJS style dynamic imports.
        // Removing this will cause deprecation warnings to appear.
        test: /(\\|\/)@angular(\\|\/)core(\\|\/).+\.js$/,
        parser: { system: true },
      },
      {
        test: /.node$/,
        use: 'node-loader',
        },
    ]
  },
  plugins: [
    new webpack.ContextReplacementPlugin(
      // fixes WARNING Critical dependency: the request of a dependency is an expression
      /(.+)?angular(\\|\/)core(.+)?/,
      path.join(__dirname, 'src'), // location of your src
      {} // a map of your routes
    ),
    new webpack.ContextReplacementPlugin(
      // fixes WARNING Critical dependency: the request of a dependency is an expression
      /(.+)?express(\\|\/)(.+)?/,
      path.join(__dirname, 'src'),
      {}
    )
  ],
  
};


// const path = require('path');
// const webpack = require('webpack');

// module.exports = {
//   entry: { server: './server.ts',
// 	prerender: './prerender.ts' },
//   resolve: { extensions: ['.js', '.ts'] },
//   target: 'node',
//   mode: 'none',
//   externals: [/node_modules/, { canvas: {}} ],
//   // externals : { canvas: {} },
//   output: {
// 	libraryTarget: 'commonjs2',
//     path: path.join(__dirname, 'dist'),
//     filename: '[name].js'
//   },
//   module: {
//     rules: [{ test: /\.ts$/, loader: 'ts-loader' }]
//   },
//   plugins: [
//     new webpack.ContextReplacementPlugin(
//       /(.+)?angular(\\|\/)core(.+)?/,
//       path.join(__dirname, 'src'),
//       {}
//     ),
//     new webpack.ContextReplacementPlugin(
//       /(.+)?express(\\|\/)(.+)?/,
//       path.join(__dirname, 'src'),
//       {}
//     )
//   ]
// };
